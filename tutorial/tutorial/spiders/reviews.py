import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class AuthorsSpider(CrawlSpider):
    name = 'reviews'
    allowed_domains = ['books.toscrape.com']
    start_urls = ['https://books.toscrape.com/']

    rules = (
        Rule(LinkExtractor(restrict_xpaths=('//article[@class="product_pod"]/div/a')), callback='parse_item', follow=False),
        Rule(LinkExtractor(allow=(r'catalogue/page-[0-9]+\.html')), follow=True),
    )

    def parse_item(self, response):
        item = {}
        item['upc'] = response.xpath('//table[@class="table table-striped"]//tr/th[contains(text(), "UPC")]/following-sibling::td/text()').extract_first()
        return item
