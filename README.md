# Preparation #

### Python Environment for Mac ###

* You need Python 3.x. Run on terminal: ```python```
* Check version. To quit from interpreter, run: ```quit()```
* If your Python is version 2.x, install [Anaconda Python for Mac](https://docs.anaconda.com/anaconda/install/mac-os/)
* When installing Anaconda, you can also get PyCharm (Python IDE). Or you can skip and use Python friendly editor (e.g. Atom). We will use  Atom editor in this session.
* In Terminal, run the following:
```
> conda list python
# packages in environment at /Users/lkusnadi/opt/anaconda3:
#
# Name                    Version                   Build  Channel
ipython                   7.16.1           py38h5ca1d4c_0  
ipython_genutils          0.2.0                    py38_0  
msgpack-python            1.0.0            py38h04f5b5a_1  
python                    3.8.3                h26836e1_2  
python-dateutil           2.8.1                      py_0  
python-jsonrpc-server     0.3.4                      py_1  
python-language-server    0.34.1                   py38_0  
python-libarchive-c       2.9                        py_0  
python.app                2                       py38_10
```
* Choose Python version (example above is: 3.8.3), then run:
```
> conda create -n web-mining -y python==3.8.3
> conda activate web-mining
> python
```
* Confirm your python is now Python 3.x


### Python Environment for Window ###

* You need Python 3.x. Run on Command Line: ```python```
* Check version. To quit from interpreter, run: ```quit()```
* If your Python is version 2.x, install [Anaconda Python for Window](https://docs.anaconda.com/anaconda/install/windows/)
* When installing Anaconda, you can also get PyCharm (Python IDE). Or you can skip and use Python friendly editor (e.g. Atom). We will use  Atom editor in this session.
* In Command Line, run the following:
```
> conda list python
# packages in environment at c:\Users\lkusnadi\opt\anaconda3:
#
# Name                    Version                   Build  Channel
ipython                   7.16.1           py38h5ca1d4c_0  
ipython_genutils          0.2.0                    py38_0  
msgpack-python            1.0.0            py38h04f5b5a_1  
python                    3.8.3                h26836e1_2  
python-dateutil           2.8.1                      py_0  
python-jsonrpc-server     0.3.4                      py_1  
python-language-server    0.34.1                   py38_0  
python-libarchive-c       2.9                        py_0  
python.app                2                       py38_10
```
* Choose Python version (example above is: 3.8.3), then run:
```
> conda create -n web-mining -y python==3.8.3
> conda activate web-mining
> python
```
* Confirm your python is now Python 3.x

### Read Python Crash Course###

If you have no idea about Python, read [Python Crash](https://bitbucket.org/autotrader-lkusnadi/web-mining/src/master/python-crash.md)

### XPath Reference###

If you want to familiarise yourself with XPath, read [XPath Cheetsheet](https://devhints.io/xpath)

### Start Web Mining ###

[Web Mining Course](https://bitbucket.org/autotrader-lkusnadi/web-mining/src/master/web-mining.md)
