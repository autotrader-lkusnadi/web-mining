# Python Crash Course #

### Python Variables ###
* Assign integer ```number = 2```
* Assign float ```cost = 2.11```
* Assign string ```name = 'Ale'```

### Printing ###

Below prints the same thing:

```python
name = 'Genghis Khan'
print('name is', name)
print('name is {}'.format(name))
print('name is {my_name}'.format(my_name=name))
print(f'name is {name}')

# output: name is Genghis Khan
```

### Condition and Loops ###
```python
if True:
  print('yes')
else:
  print('no')

if True:
  print('yes')
elif number > 0:
  print('positive')
else:
  print('no idea')

while True:
  print('hello')

for x in [1 ,2 ,3]:
  print(x)
```

### Python Data Structure ###
* List of the same items ```numbers = [1, 2, 3]```
* List of mixed items ```things = [1, 'cow', 2.1]```
* Dictionary or map ```caches = {'key1': 'value1', 'key2': 'value2'}```

### Working with List ###
```python
numbers = [1, 2, 3]
numbers[0] # 1
numbers[-1] # 3

doubles = [2 * x for x in numbers] # [2, 4, 6]

numbers.append(4) # [1, 2, 3, 4]

extras = [4, 5]
numbers.extend(extras) # [1, 2, 3, 4, 5, 6]
```

### Working with Dictionary ###
```python
book = {'title': 'Little Pig', 'cost': 2.33}
book.get('title') # Little Pig
book['title'] # Little Pig
book.get('color') # Returns None
book.setdefault('color', 'blue') # adding color as key and blue as value
```

### Functions ###
* Simple function (strong typed vs weak typed)
```python
def print_name(name: str):
   print(f'name is {name}')

def print_name(name):
   print(f'name is {name}')

print_name('Harry')
```
* Function with return
```python
def get_name():
  return 'Bob'

name = get_name()

def origin_coordinate():
  return 0, 0

x, y = origin_coordinate()
```

### Class ###
```python
class Pig:
  name = 'Babe'

  def __init__(self, my_name):
    self.name = my_name

  def oink(self):
    print('oink')

one = Pig()
print(one.name) # Babe
one.oink() # oink

two = Pig('Arl')
print(two.name) # Arl
```

### Generator ###
Demonstrate how to use yield as generator.

```python
def drinks():
  a = 1
  return ['soda', 'coke']

for x in drinks():
  print(x) # a is assigned twice, each loop runs from top to bottom in drinks()

def food():
  a = 1
  yield 'lasagna'
  yield 'ham'

for x in food():
  print(x) # a is assigned once, second yield continue from the first yield
```
