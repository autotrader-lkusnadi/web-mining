# Web Mining Course #

### Create Python Environment ###
* If you haven't created your environment, follow [README.md](https://bitbucket.org/autotrader-lkusnadi/web-mining/src/master/README.md)
* Activate your python environment specific for web-mining using terminal or command line.
```
conda activate web-mining
```
* Install modules for scrapy once you are in your environment. You will see ```(web-mining)``` on your prompt, showing the environment.
```
pip install -r requirements-local.txt
```
* Check if Scrapy is installed, we are using Scrapy 2.5.0
```
scrapy version
```

### Create Scrapy Project ###
* Create project called "tutorial"
```
scrapy startproject tutorial
cd tutorial
```
* We are going to use this [website](https://books.toscrape.com/) for our test, let's create a general spider
```
scrapy genspider reviews books.toscrape.com -t crawl
atom tutorial/spiders/authors.py
```

### Discuss Project Structure ###
* Types of spider in parent class (CrawlSpider, Spider, SitemapSpider, etc.) (doc)[https://docs.scrapy.org/en/latest/topics/spiders.html]
* name is what we use to call the spider
* allowed_domains and start_urls must be in sync, we can include multiple domains and/or starting URLs
* Navigation are anchor links ```<a href=.../>```
* Callback function(s) holds the logic to scrape the data

### Understand the Site ###
*  Where the data lives (e.g. the books from the list) and understand common pattern to get there
    *  Each item has common pattern ```//article[@class="product_pod"]```
    *  Refine: ```//article[@class="product_pod"]/h3/a```
    *  But you can also do: ```//article[@class="product_pod"]/div/a```
*  How to navigate the list
    *  Inspect navigation button between pages
    *  Lock down to a specific pattern ```//li[@class="next"]/a```
*  What data to get (e.g. the book detail)
    *  Data is in rows of a table ```//table[@class="table table-striped"]//tr```
    *  Get UPC value, be as specific as possible, hints: ```//table[@class="table table-striped"]//tr/th[contains(text(), "UPC")]```
    *  How to get the value? hints: See X-Path Cheatsheet, look for ```following-sibling```
    *  Get Review, figure out yourself :)
*  NOTE: Be as specific as you can, do as much as you can in locating the DOM elements

### Traversing the List ###
* Your reviews.py code:
```python
class AuthorsSpider(CrawlSpider):
    name = 'reviews'
    allowed_domains = ['books.toscrape.com']
    start_urls = ['https://books.toscrape.com/']

    rules = (
        Rule(LinkExtractor(restrict_xpaths=('//article[@class="product_pod"]/div/a')), callback='parse_item', follow=False),
    )

    def parse_item(self, response):
        self.logger.info(response.url)
        item = {}
        return item
```
* Run: ```scrapy crawl reviews```
* Output:
```
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/our-band-could-be-your-life-scenes-from-the-american-indie-underground-1981-1991_985/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/its-only-the-himalayas_981/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/olio_984/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/scott-pilgrims-precious-little-life-scott-pilgrim-1_987/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/starving-hearts-triangular-trade-trilogy-1_990/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/mesaerion-the-best-science-fiction-stories-1800-1849_983/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/rip-it-up-and-start-again_986/index.html
2021-08-17 22:41:02 [reviews] INFO: https://books.toscrape.com/catalogue/libertarianism-for-beginners_982/index.html
```

### Getting the Item Detail ###
* Your reviews.py code:
```python
class AuthorsSpider(CrawlSpider):
    name = 'reviews'
    allowed_domains = ['books.toscrape.com']
    start_urls = ['https://books.toscrape.com/']

    rules = (
        Rule(LinkExtractor(restrict_xpaths=('//article[@class="product_pod"]/div/a')), callback='parse_item', follow=False),
    )

    def parse_item(self, response):
        item = {}
        item['upc'] = response.xpath('//table[@class="table table-striped"]//tr/th[contains(text(), "UPC")]/following-sibling::td').extract_first()
        return item
```
* Run spider and output to files (csv, json, jl): ```spider crawl reviews -o reviews.csv```
* We got problem!!! Data is captured with tags.
* Fixing: ```response.xpath('//table[@class="table table-striped"]//tr/th[contains(text(), "UPC")]/following-sibling::td/text()').extract_first()```
* My point: do most of things in DOM selection, reduce your code

### Handle Navigation ###
* We did our homework before about XPath to locate the navigation link. Let's assume we get lazy, use the link pattern instead.
* The navigation has pattern: ```catalogue/page-digits.html```
* Add extra rules in your reviews.py code:
```python
rules = (
    Rule(LinkExtractor(restrict_xpaths=('//article[@class="product_pod"]/div/a')), callback='parse_item', follow=False),
    Rule(LinkExtractor(allow=(r'catalogue/page-[0-9]+\.html')), follow=True),
)
```
* Re-run spider:
```
rm -fv reviews.csv
spider crawl reviews -o reviews.csv
```

### Homework ###
* Get UPC, Price, Tax, and Availability
* Get the ASMR of scraping

### Scraping Pipeline ###
* Do something additional like: get data from site and put it in DB, data sanitation, etc.
* Let's do something simple and ridiculous: we don't want any upc starting with 'f'
* Define an Item class, why? This is your schema
* In tutorial/items.py:
```python
import scrapy


class TutorialItem(scrapy.Item):
    upc = scrapy.Field()
```
* In tutorial/pipelines.py:
```python
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem


class TutorialPipeline:
    def process_item(self, item, spider):
        adapter = ItemAdapter(item)
        if adapter.get('upc').startswith('f'):
            raise DropItem(f'UPC starts with f for {item}')
        return item
```
* In tutorial/settings.py, enable the pipeline:
```python
ITEM_PIPELINES = {
   'cpo_crawlers.pipelines.CpoCrawlersPipeline': 300,
}
LOG_LEVEL = 'INFO'
```
* Run the crawl ```spider crawl reviews -o reviews-2.csv```


### Debugging ###
* When you need to debug, use ```scrapy shell https://books.toscrape.com/catalogue/el-deafo_691/index.html```
* This will give you object level access to do testing before scraping the entire site
* Respect robots.txt or be banned during development

### Productionize ###
* Open an account with Zyte (these guys maintain scrapy framework)
* Install shub in your environment ```pip install shub```
* Create project and get the project ID
* Pass project ID and API key to shub with ```shub login```
* Deploy your spider ```shub deploy```
* Zyte has built-in randomness
* Spend $$$ to get Crawlera (random proxy to avoid getting banned)
* Spend more $$$ to overcome issue like SPA page with full javascript dynamic DOM
* Collected data will be available as RESTful API in Zyte
